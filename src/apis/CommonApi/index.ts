import http from '@/utils/requestUtil'
 
export default {
  /**
   * 获取配置项列表
   */
  getConfigItemList() {
    return http.get(`/sz-xxxxxx/api/getConfigItemList`)
  },
}
