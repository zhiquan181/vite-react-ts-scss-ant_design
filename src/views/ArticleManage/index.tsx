import { useRef, useState } from 'react'
import { Button } from 'antd'
import style from './index.module.scss'
import Child from './comments/Child'
 
type IStringProp = string
type IChildRefProp = { text: string; fn: () => void; }
 
const ArticleManage = () => {
  // 数值
  const [count, setCount] = useState(0)
 
  // 子组件实例
  const childRef = useRef(null)
 
  // 获取子组件的变量
  const handleChildOnChange = (value: IStringProp) => {
    console.log('handleChildOnChange ->', value)
  }
 
  // 加一
  const handleIncrementCount = () => {
    setCount(count + 1)
  }
 
  // 点击搜索事件
  const handleButtonOnClick = () => {
    const childRefCurrent = childRef?.current
    if (childRefCurrent) {
      console.log('handleButtonOnClick ->', childRefCurrent)
      const current: IChildRefProp = childRefCurrent
      current.fn()
    }
  }
 
  return (
    <>
      <div className={style.container}>
        <p style={{ fontSize: '20px' }}>文章管理页面</p>
        <h1>计数器：{count}</h1>
        <Child ref={childRef} count={count} handleIncrementCount={handleIncrementCount} onChange={handleChildOnChange} />
        <Button
          type="primary"
          size="small"
          onClick={handleButtonOnClick}
        >
          搜索
        </Button>
      </div>
    </>
  )
}
 
export default ArticleManage
