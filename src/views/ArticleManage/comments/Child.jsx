import { useState, forwardRef, useImperativeHandle } from 'react'
import { message, Input, Button } from 'antd'
 
const Child = forwardRef((props, ref) => {
 
  // 通过props属性来访问这些变量和方法
  const { count } = props
 
  // 文本
  const [text, setText] = useState('520')
 
  // 改变输入框文本方法
  const handleTextOnChange = (event) => {
    const value = event.target.value
    setText(value)
    props.onChange(value) // 调用父组件传递的回调函数
  }
 
  // fn方法
  const fn = () => {
    console.log('well ~')
  }
 
  // 将子组件的 fn 方法暴露给父组件
  useImperativeHandle(ref, () => ({
    text,
    fn
  }))
 
  return (
    <>
      <p onClick={props.handleIncrementCount}>Hello{count}</p>
      <Input
        size='small'
        value={text}
        placeholder="请输入文本"
        spellCheck="false"
        allowClear
        onChange={val => handleTextOnChange(val)}
      />
    </>
  )
})
 
export default Child
