import { useEffect } from 'react'
import { message, Input, Button } from 'antd'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import CommonApi from '@/apis/CommonApi'
import { userInfoState } from '@/stores/ILoveYouStore'
 
const SystemPanel = () => {
  // 在Recoil中，使用 useRecoilValue 来获取最新的状态值
  const userInfoStateValue = useRecoilValue(userInfoState)
 
  // 在Recoil中，使用 useSetRecoilState 来更新状态
  const setUserInfoState = useSetRecoilState(userInfoState)
 
  // 获取配置项列表
  const getConfigItemList = async () => {
    const res = await CommonApi.getConfigItemList()
    console.log('getConfigItemList ->', res)
  }
 
  // 副作用
  useEffect(() => {
    getConfigItemList()
  })
 
  return (
    <div style={{
      display: 'flex',
      flexDirection: 'column',
      width: '100%',
      height: '100%',
      overflow: 'hidden',
      alignItems: 'center',
      justifyContent: 'center',
    }}>
      <p>Welcome to the page of SystemPanel!</p>
      <p>{JSON.stringify(userInfoStateValue)}</p>
      <Button type="primary" onClick={
        () => {
          const userInfo = {
            username: '帅龍之龍',
            height: '180CM',
            weight: '140KG',
          }
          setUserInfoState(userInfo)
        }
      }>Primary</Button>
    </div>
  )
}
 
export default SystemPanel
