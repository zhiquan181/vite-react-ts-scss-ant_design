import { atom }from 'recoil'
 
const userInfoState = atom({
  key: 'count',
  default: {},
})

export {
  userInfoState
}
