import SystemPanel from '@/views/SystemPanel/index.tsx'
import ArticleManage from '@/views/ArticleManage/index.tsx'
import Help from '@/views/Help/index.tsx'
 
const routers = [
  {
    path: '/',
    name: '系统面板',
    components: SystemPanel
  },
  {
    path: '/ArticleManage',
    name: '文章管理',
    components: ArticleManage
  },
  {
    path: '/help',
    name: '帮助中心',
    components: Help
  }
]
 
export default routers