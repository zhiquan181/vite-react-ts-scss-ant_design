import React from 'react'
import ReactDOM from 'react-dom/client'
import { BrowserRouter as Router } from 'react-router-dom'
import { RecoilRoot } from 'recoil';
import App from './App.tsx'
 
/**
 * 报错：useNavigate() 钩子函数只能在 <Router> 组件的上下文中使用。当你尝试在 <Router> 组件的上下文之外使用 useNavigate() 时，会收到 "useNavigate() may be used only in the context of a Router component" 的错误提示。
 * 解决：为了解决这个问题，你需要确保你的应用已经被 <Router> 组件所包裹，最佳的做法是将 <Router> 组件包裹在你的应用的根组件周围。
 */
ReactDOM.createRoot(document.getElementById('root')!).render(
  // 说明：React.StrictMode模式下，useEffect会在组件挂载和卸载时各执行一次。
  <RecoilRoot>
    <Router>
      <App />
    </Router>
  </RecoilRoot>
)
