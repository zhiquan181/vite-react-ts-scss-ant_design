import React, { useState } from 'react'
import { HomeOutlined, ProfileOutlined, DeploymentUnitOutlined, CodeSandboxOutlined } from '@ant-design/icons'
import type { MenuProps } from 'antd'
import { Menu } from 'antd'
import { Route, Routes, Link, useNavigate } from 'react-router-dom'
import routers from '@/router/index.js'
import style from './App.module.scss'
 
type MenuItem = Required<MenuProps>['items'][number]
 
function getItem(
  label: React.ReactNode,
  key: React.Key,
  icon?: React.ReactNode,
  children?: MenuItem[],
  type?: 'group',
): MenuItem {
  return { key, icon, children, label, type } as MenuItem
}
 
const items: MenuProps['items'] = [
  getItem('系统面板', 'SystemPanel', <HomeOutlined />),
  getItem('文章管理', 'ArticleManage', <ProfileOutlined />),
  getItem('前端开发技术栈', 'sub1', <DeploymentUnitOutlined />, [
    getItem('Vue.js', 'g1', null, [
      getItem('ElementUI', '11111'),
      getItem('ElementPlus', '22222')
    ], 'group'),
    getItem('React', 'g2', null, [
      getItem('AntDesign', '33333'),
      getItem('MUI', '44444')
    ], 'group'),
    getItem('Angular', 'g3', null, [
      getItem('DevUI', '55555')
    ], 'group'),
  ]),
  { type: 'divider' },
  getItem('后端开发技术栈', 'sub2', <CodeSandboxOutlined />, [
    getItem('Java', 'g4', null, [
      getItem('SpringBoot', '66666'),
      getItem('SpringCloud', '77777')
    ], 'group'),
    getItem('Database', 'g5', null, [
      getItem('MySQL', '88888'),
      getItem('Redis', '99999'),
    ], 'group'),
  ]),
  { type: 'divider' },
]
 
const App: React.FC = () => {
 
  // 系统名称
  const [systemName, setSystemName] = useState('FullStack')
 
  // 导航函数
  const navigate = useNavigate()
 
  // 菜单点击事件
  const onClick: MenuProps['onClick'] = (e) => {
    console.log('handleMenuOnClick -> ', e)
    if (e.key === 'SystemPanel') {
      navigate('')
    } else {
      navigate(e.key)
    }
  }
  
  return (
    <div className={style.appContainer}>
      {/* 侧边栏 */}
      <div className={style.aside}>
        <div className={style.logo} onClick={() => {
          setSystemName(`FullStack v ${parseInt((Math.random() * 10000).toString())}`)
        }}>
          {systemName}
        </div>
        <div className={style.navigate}>
          <Menu
            mode="inline"
            style={{ width: 200 }}
            items={items}
            defaultSelectedKeys={['SystemPanel']} // 初始选中的菜单项 key 数组
            defaultOpenKeys={['sub1', 'sub2']} // 初始展开的 SubMenu 菜单项 key 数组
            onClick={onClick}
          />
        </div>
        <div style={{ textAlign: 'center' }}>
            <Link to={'help'} key={'help'} style={{ 
              display: 'block',
              padding: '7px 0',
              borderTop: '1px solid #eee',
              textDecoration: 'none',
              fontSize: '14px',
              color: '#000',
             }}>帮助中心</Link>
          </div>
      </div>
 
      {/* 内容区 */}
      <div className={style.content}>
        <Routes>
          {
            routers.map((item: any, index: number) => (
              <Route path={item.path} key={index} element={<item.components />}></Route>
            ))
          }
        </Routes>
      </div>
    </div>
  )
}
 
export default App
