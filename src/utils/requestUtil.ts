import axios from 'axios'
 
const http = axios.create({
  baseURL: '',
  timeout: 3800 * 1000, // 请求超时时间设置为300秒
})
 
/**
 * 请求拦截器
 */
http.interceptors.request.use(
  (req) => {
    // console.log('请求拦截器 =>', req)
    return req
  },
  (error) => {
    // console.error('请求失败 =>', error)
    return Promise.reject(error)
  }
)
 
/**
 * 响应拦截器
 */
http.interceptors.response.use(
  (res) => {
    // console.log('响应拦截器 =>', res)
    if (res.status == 200) {
      return res.data
    } else {
      // console.error('响应错误 =>', '状态码为' + res.status)
      return { success: false, msg: '响应错误，状态码为' + res.status }
    }
  },
  (error) => {
    // console.error('响应失败 =>', error)
    return { success: false, msg: error }
  }
)
 
export default http
