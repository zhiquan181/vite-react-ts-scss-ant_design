import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react-swc'
import { resolve } from 'path'
 
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  resolve: {
    alias: {
      '@': resolve(__dirname, './src'),
    }
  },
  server: {
    host: '0.0.0.0',
    open: false,
    proxy: {
      // /sz-xxxxxx/api 服务端
      '/sz-xxxxxx/api': {
        target: 'https://xxxxxx.com/',
        changeOrigin: true,
        secure: false, // 解决 Error: unable to verify the first certificate 报错
        ws: false,
      }
    }
  },
})