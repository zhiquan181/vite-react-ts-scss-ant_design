### 1.React和Vue的区别
```
同：
（1）数据驱动视图
（2）使用虚拟DOM操作真实DOM
（3）组件化开发
异：
（1）React使用`jsx/tsx`靠近js/ts，Vue使用`template`靠近html
（2）React是函数式编程，Vue声明式编程
（3）React自由发挥区间比较大（给的是核心）， Vue实现的Api比较全面
```

### 2.类组件和函数组件的区别
```
（1）类组件有生命周期，函数组件没有生命周期，需要调用Hooks函数
（2）类组件需要实例化，用this来指向组件本身，而函数组件不需要实例化，没有this，函数组件渲染时候只需要调用函数就可以了，执行效率高
（3）类组件继承于component类，内部必须有render方法，必须return一个react元素。而函数组件是一个函数，返回一个jsx元素，使用起来比较简单。
```

### 3.React 的 useState 有什么作用
```
（1）useState给函数式组件引入组件状态，返回值是一个数组，第一个参数是组件状态，第二个参数是更新组件状态的函数。
（2）组件状态接受一个初始值。
```

### 4.React 的 useEffect 有什么作用
```
（1）useEffect 是 React 中的一个内置钩子函数，主要用于在函数组件中执行具有“副作用”的操作，例如从 API 中获取数据或手动更新 DOM。
（2）模拟生命周期方法：如果你熟悉 React 类的生命周期函数，你可以把 useEffect 看作是 componentDidMount，componentDidUpdate 和 componentWillUnmount 这三个函数的组合。
（3）它有两个参数，第一个参数是回调函数，第二个是依赖项，只有依赖项发生变化的时候，回调函数才会执行。
```
 
### 5.React 的 useContext 有什么作用
```
用于在组件之间共享数据。它允许您在组件树中传递数据，而不必将数据通过props一层层地传递下去。
```
 
### 6.React 的 useReducer 有什么作用
```
用于管理组件的状态(state)。它可以用来替代 `useState`，特别是在需要处理复杂状态逻辑的场景下，使用 `useReducer` 可以更好地组织代码。
```

### 7.React 的 useCallback 有什么作用
```
在依赖不变的情况下，防止函数的重新创建，从而避免不必要的渲染。
```

### 8.React 的 useMemo 有什么作用
```
在依赖不变的情况下返回旧数据的值，而不是重新计算数据。这样可以减少数据的查找和校验依赖是否改变的开销，提高程序的运行效率。
```

### 9.React 的 useRef 有什么作用
```
常用于保存对DOM元素或组件实例的引用。
```

### 10.React 的 useImperativeHandle 有什么作用
```
允许子组件自定义暴露属性或方法给父组件。
```
### 11.React 有哪些常用的钩子函数
```
useState ：用于在函数组件中使用状态。
useEffect ：用于在函数组件中执行副作用操作。
useContext ：用于在函数组件中使用上下文。
useReducer ：用于在函数组件中使用 Redux 风格的管理状态。
useCallback ：用于在函数组件中缓存回调函数。
useMemo ：用于在函数组件中缓存计算结果。
useRef ：用于在函数组件中存储/查找组件内的标签或任意其它数据。
useImperativeHandle ：用于在函数组件中自定义暴露给父组件的实例值。
```
